package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI extends JPanel implements ActionListener {
    private JTextField kodeField;
    private JTextField namaField;
    private JTextField sksField;
    private JTextField kapasitasField;
    private JButton tambah;
    private JButton kembali;


    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mata Kuliah
        this.setBorder(new EmptyBorder(30, 0, 0, 0)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // Title label "Tambah Mata Kuliah"
        JLabel titleLabel = new JLabel("Tambah Mata Kuliah");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // Label "Kode Mata Kuliah"
        JLabel kodeLabel = new JLabel("Kode Mata Kuliah:");
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(kodeLabel, this);

        // TextField for kode matkul
        kodeField = new JTextField();
        kodeField.setMaximumSize(new Dimension(250, 30));
        kodeField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(kodeField, this);

        // Label "Kode Mata Kuliah"
        JLabel namaLabel = new JLabel("Nama Mata Kuliah:");
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(namaLabel, this);

        // TextField for nama matkul
        namaField = new JTextField();
        namaField.setMaximumSize(new Dimension(250, 30));
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(namaField, this);

        // Label "SKS"
        JLabel sksLabel = new JLabel("SKS:");
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(sksLabel, this);

        // TextField for sks
        sksField = new JTextField();
        sksField.setMaximumSize(new Dimension(250, 30));
        sksField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(sksField, this);

        // Label "Kapasitas"
        JLabel kapasitasLabel = new JLabel("Kapasitas:");
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(kapasitasLabel, this);

        // TextField for kapasitas
        kapasitasField = new JTextField();
        kapasitasField.setMaximumSize(new Dimension(250, 30));
        kapasitasField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(kapasitasField, this);

        // List of all buttons
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(tambah = new JButton("Tambahkan"));
        tambah.setBackground(Color.decode("#abc32f")); // set color for tambah button
        buttons.add(kembali = new JButton("Kembali"));
        kembali.setBackground(Color.decode("#4890a8")); // set color for kembali button


        this.add(Box.createRigidArea(new Dimension(0,20))); // add gap between text field and button
        // Iterate each button, add spesifications
        for (int i=0; i<buttons.size(); i++) {
            (buttons.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (buttons.get(i)).setFocusable(false);
            (buttons.get(i)).setForeground(Color.WHITE);
            (buttons.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            (buttons.get(i)).addActionListener(this);
            this.add(buttons.get(i), this);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ArrayList<JTextField> textfields = new ArrayList<JTextField>();
        textfields.add(kodeField);
        textfields.add(namaField);
        textfields.add(sksField);
        textfields.add(kapasitasField);

        if (e.getSource()==kembali) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
            // clear text fields
            for (int i=0; i<textfields.size(); i++) {
                (textfields.get(i)).setText("");
            }
        } else if (kodeField.getText().equals("")||namaField.getText().equals("")||sksField.getText().equals("")||kapasitasField.getText().equals("")) {
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
        } else {
            if (e.getSource()==tambah) {
                String kode = kodeField.getText();
                String nama = namaField.getText();
                int sks = Integer.parseInt(sksField.getText());
                int kapasitas = Integer.parseInt(kapasitasField.getText());

                if (SistemAkademikGUI.isMatkulExist(nama)) {
                    JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama));
                    // clear text fields
                    for (int i=0; i<textfields.size(); i++) {
                        (textfields.get(i)).setText("");
                    }
                } else {
                    MataKuliah matakuliah = new MataKuliah(kode, nama, sks, kapasitas);
                    SistemAkademikGUI.addMatkul(matakuliah);
                    JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s berhasil ditambahkan", nama));
                    // clear text fields
                    for (int i=0; i<textfields.size(); i++) {
                        (textfields.get(i)).setText("");
                    }
                }
            }
        }
    }
    
}
