package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI extends JPanel implements ActionListener {
    private JButton selesai;

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.setBorder(new EmptyBorder(50, 0, 0, 0)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // Title label "Detail Ringkasan Mahasiswa"
        JLabel titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // List of all other labels
        ArrayList<JLabel> labels = new ArrayList<JLabel>();
        labels.add(new JLabel(String.format("Nama: %s", mahasiswa.getNama())));
        labels.add(new JLabel(String.format("NPM: %d", mahasiswa.getNpm())));
        labels.add(new JLabel(String.format("Jurusan: %s", mahasiswa.getJurusan())));
        labels.add(new JLabel("Daftar Mata Kuliah:"));

        // List daftar mata kuliah; handle if empty
        if (mahasiswa.getBanyakMatkul()==0) {
            labels.add(new JLabel("<html><b>Belum ada mata kuliah yang diambil</b></html>"));
        } else {
             // List daftar mata kuliah (sorted)
            String[] listMatkul = sortMatkul(mahasiswa, mahasiswa.getMataKuliah());
            String result = "";
            int j = 1;
            for (int i=0; i<listMatkul.length; i++) {
                result += String.format("<b>%d. %s</b><br>", j++, listMatkul[i]);
            }
            labels.add(new JLabel("<html>"+result+"</html>"));
        }
        
        labels.add(new JLabel(String.format("Total SKS: %d", mahasiswa.getTotalSKS())));
        labels.add(new JLabel("Hasil Pengecekan IRS"));

        // List IRS; handle if empty
        mahasiswa.cekIRS();
        if (mahasiswa.getBanyakMasalahIRS()==0) {
            labels.add(new JLabel("<html><b>IRS tidak bermasalah</b></html>"));
        } else {
            String[] listIRS = mahasiswa.getMasalahIRS();
            String irs = "";
            int j = 1;
            for (int i=0; i<listIRS.length; i++) {
                if (listIRS[i]==null) break;
                irs += String.format("<b>%d. %s</b><br>", j++, listIRS[i]);
            }
            labels.add(new JLabel("<html>"+irs+"</html>"));
        }

        // add spesification to each labels; add to panel
        for (int i=0; i<labels.size(); i++) {
            (labels.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (labels.get(i)).setHorizontalAlignment(SwingConstants.CENTER);
            (labels.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
            this.add(labels.get(i), this);
        }

        // Button selesai; back to home page
        selesai = new JButton("Selesai");
        selesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesai.setFocusable(false);
        selesai.setBackground(Color.decode("#abc32f")); // set color for lihat button
        selesai.setForeground(Color.WHITE);
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        selesai.addActionListener(this);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(selesai, this);
    }

    private void alphabetSort(String[] arr) {
        String temp;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                // compare one string with other strings
                if (arr[i].compareTo(arr[j]) > 0) {
                    // swap
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    private String[] sortMatkul(Mahasiswa mahasiswa, MataKuliah[] listMatkul) {
        String[] temp = new String[mahasiswa.getBanyakMatkul()];
        for (int i=0; i<listMatkul.length; i++) {
            if (listMatkul[i]==null) break;
            temp[i] = listMatkul[i].getNama();
        }

        if (temp.length > 1) {
            alphabetSort(temp);
        }
        return temp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==selesai) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
        }
    }
}
