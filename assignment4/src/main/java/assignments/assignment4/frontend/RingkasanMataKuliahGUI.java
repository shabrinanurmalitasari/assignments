package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI extends JPanel implements ActionListener {
    private String[] namaList;
    private JComboBox<String> namaCb;
    private JButton lihat;
    private JButton kembali;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {

        this.setBorder(new EmptyBorder(150, 100, 100, 100)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // sorts matakuliah by name
        namaList = new String[daftarMataKuliah.size()];
        for (int i=0; i<daftarMataKuliah.size(); i++) {
            namaList[i] = daftarMataKuliah.get(i).getNama();
        }
        alphabetSort(namaList);

        // Title label "Ringkasan Mata Kuliah"
        JLabel titleLabel = new JLabel("Ringkasan Mata Kuliah");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // Label "Pilih Nama Matkul"
        JLabel npmLabel = new JLabel("Pilih Nama Matkul");
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(npmLabel, this);

        // Combo box npm
        namaCb = new JComboBox<String>(namaList);
        namaCb.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaCb.setMaximumSize(new Dimension(200, 30));
        namaCb.addActionListener(this);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(namaCb, this);

        // List of all buttons
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(lihat = new JButton("Lihat"));
        lihat.setBackground(Color.decode("#abc32f")); // set color for lihat button
        buttons.add(kembali = new JButton("Kembali"));
        kembali.setBackground(Color.decode("#4890a8")); // set color for kembali button

        this.add(Box.createRigidArea(new Dimension(0,20))); // add gap between text field and button
        // Iterate each button, add spesifications
        for (int i=0; i<buttons.size(); i++) {
            (buttons.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (buttons.get(i)).setFocusable(false);
            (buttons.get(i)).setForeground(Color.WHITE);
            (buttons.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            (buttons.get(i)).addActionListener(this);
            this.add(buttons.get(i), this);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
        }

    }

    private void alphabetSort(String[] arr) {
        String temp;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                
                // to compare one string with other strings
                if (arr[i].compareTo(arr[j]) > 0) {
                    // swapping
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    private MataKuliah getMataKuliah(String nama) {
        ArrayList<MataKuliah> daftarMataKuliah = SistemAkademikGUI.getDaftarMatakuliah();
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource()==kembali) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
        } else if (namaCb.getItemCount()==0) {
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
        } else {
            if (e.getSource()==lihat) {
                String nama = (String) namaCb.getSelectedItem();
                MataKuliah matakuliah = getMataKuliah(nama);
                (SistemAkademikGUI.panel).add(new DetailRingkasanMataKuliahGUI(SistemAkademikGUI.frame, matakuliah, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "detail matkul");
                (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
                (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "detail matkul");
            }
        }
    }
}
