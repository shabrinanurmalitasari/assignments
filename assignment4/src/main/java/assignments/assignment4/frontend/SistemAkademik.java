package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame {
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);
    public static CardLayout cl = new CardLayout();
    public static JPanel panel = new JPanel();
    public static JFrame frame;

    public SistemAkademikGUI(){

        // Membuat Frame
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        // TODO: Tambahkan hal-hal lain yang diperlukan
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setLayout(new BorderLayout());

        panel.setLayout(cl);
        panel.add(new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah), "home");
        frame.add(panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static ArrayList<Mahasiswa> getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public static ArrayList<MataKuliah> getDaftarMatakuliah() {
        return daftarMataKuliah;
    }

    public static JFrame getFrame() {
        return frame;
    }

    public static void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa.add(mahasiswa);
    }

    public static void addMatkul(MataKuliah matakuliah) {
        daftarMataKuliah.add(matakuliah);
    }

    public static boolean isNpmExist(long npm) {
        for (int i=0; i<daftarMahasiswa.size(); i++) {
            if (daftarMahasiswa.get(i).getNpm() == npm) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMatkulExist(String nama) {
        for (int i=0; i<daftarMataKuliah.size(); i++) {
            if (daftarMataKuliah.get(i).getNama().equals(nama)) {
                return true;
            }
        }
        return false;
    }
}
