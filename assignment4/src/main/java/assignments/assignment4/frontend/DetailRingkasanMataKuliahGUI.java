package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI extends JPanel implements ActionListener {
    private JButton selesai;

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.setBorder(new EmptyBorder(40, 0, 0, 0)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // Title label "Detail Ringkasan Mata Kuliah"
        JLabel titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // List of all other labels
        ArrayList<JLabel> labels = new ArrayList<JLabel>();
        labels.add(new JLabel(String.format("Nama mata kuliah: %s", mataKuliah.getNama())));
        labels.add(new JLabel(String.format("Kode: %s", mataKuliah.getKode())));
        labels.add(new JLabel(String.format("Jumlah mahasiswa: %d", mataKuliah.getJumlahMahasiswa())));
        labels.add(new JLabel(String.format("Kapasitas: %d", mataKuliah.getKapasitas())));
        labels.add(new JLabel("Daftar Mahasiswa:"));

        // List daftar mahasiswa; handle if empty
        if (mataKuliah.getJumlahMahasiswa()==0) {
            labels.add(new JLabel("<html><b>Belum ada mahasiswa yang mengambil mata kuliah ini.</b></html>"));
        } else {
            String[] listNama = listNama(mataKuliah, mataKuliah.getDaftarMahasiswa());
            String result = "";
            int j = 1;
            for (int i=0; i<listNama.length; i++) {
                // if (listNama[i]==null) break;
                result += String.format("<b>%d. %s</b><br>", j++, listNama[i]);
            }
            labels.add(new JLabel("<html>"+result+"</html>"));
        }

        // add spesification to each labels; add to panel
        for (int i=0; i<labels.size(); i++) {
            (labels.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (labels.get(i)).setHorizontalAlignment(SwingConstants.CENTER);
            (labels.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
            this.add(labels.get(i), this);
        }

        // Button selesai; back to home page
        selesai = new JButton("Selesai");
        selesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesai.setFocusable(false);
        selesai.setBackground(Color.decode("#abc32f")); // set color for lihat button
        selesai.setForeground(Color.WHITE);
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        selesai.addActionListener(this);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(selesai, this);
    }

    private String[] listNama(MataKuliah matakuliah, Mahasiswa[] listMahasiswa) {
        String[] temp = new String[matakuliah.getJumlahMahasiswa()];
        for (int i=0; i<listMahasiswa.length; i++) {
            if (listMahasiswa[i]==null) break;
            temp[i] = listMahasiswa[i].getNama();
        }
        return temp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        if (e.getSource()==selesai) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
        }
    }
}
