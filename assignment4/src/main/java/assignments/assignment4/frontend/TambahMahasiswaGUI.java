package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI extends JPanel implements ActionListener {
    private JTextField npmField;
    private JTextField namaField;
    private JButton tambah;
    private JButton kembali;


    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        this.setBorder(new EmptyBorder(100, 0, 0, 0)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // Title label "Tambah Mahasiswa"
        JLabel titleLabel = new JLabel("Tambah Mahasiswa");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // Label "Nama"
        JLabel nama = new JLabel("Nama:");
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,20)));
        this.add(nama, this);

        // TextField for nama mahasiswa
        namaField = new JTextField();
        namaField.setMaximumSize(new Dimension(250, 30));
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(namaField, this);

        // Label "NPM"
        JLabel npm = new JLabel("NPM:");
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(npm, this);

        // TextField for npm
        npmField = new JTextField();
        npmField.setMaximumSize(new Dimension(250, 30));
        npmField.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(npmField, this);

        // List of all buttons
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(tambah = new JButton("Tambahkan"));
        tambah.setBackground(Color.decode("#abc32f")); // set color for tambah button
        buttons.add(kembali = new JButton("Kembali"));
        kembali.setBackground(Color.decode("#4890a8")); // set color for kembali button


        this.add(Box.createRigidArea(new Dimension(0,20))); // add gap between text field and button
        // Iterate each button, add spesifications
        for (int i=0; i<buttons.size(); i++) {
            (buttons.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (buttons.get(i)).setFocusable(false);
            (buttons.get(i)).setForeground(Color.WHITE);
            (buttons.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            (buttons.get(i)).addActionListener(this);
            this.add(buttons.get(i), this);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource()==kembali) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
            // clear text fields
            namaField.setText("");
            npmField.setText("");
        } else if ((namaField.getText()).equals("") || (npmField.getText()).equals("")) {
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
        } else {
            if (e.getSource()==tambah) {
                String nama = namaField.getText();
                long npm = Long.parseLong(npmField.getText());

                if (SistemAkademikGUI.isNpmExist(npm)) {
                    JOptionPane.showMessageDialog(null, String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm));
                    // clear text fields
                    namaField.setText("");
                    npmField.setText("");
                } else {
                    Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
                    SistemAkademikGUI.addMahasiswa(mahasiswa);
                    JOptionPane.showMessageDialog(null, String.format("Mahasiswa %d-%s berhasil ditambahkan", npm, nama));
                    // clear text fields
                    namaField.setText("");
                    npmField.setText("");
                }
            }
        }
    }
    
}
