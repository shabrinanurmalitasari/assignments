package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI extends JPanel implements ActionListener {
    private Long[] npmList;
    private JComboBox<Long> npmCb;
    private JButton lihat;
    private JButton kembali;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.setBorder(new EmptyBorder(150, 100, 100, 100)); // set panel's empty border
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // set panel's layout

        // sorts mahasiswa by npm
        npmList = new Long[daftarMahasiswa.size()];
        for (int i=0; i<daftarMahasiswa.size(); i++) {
            npmList[i] = daftarMahasiswa.get(i).getNpm();
        }
        insertionSort(npmList);

        // Title label "Ringkasan Mahasiswa"
        JLabel titleLabel = new JLabel("Ringkasan Mahasiswa");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(titleLabel);

        // Label "Pilih NPM"
        JLabel pilihNpm = new JLabel("Pilih NPM");
        pilihNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNpm.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(0,15)));
        this.add(pilihNpm, this);

        // Combo box npm
        npmCb = new JComboBox<Long>(npmList);
        npmCb.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmCb.setMaximumSize(new Dimension(200, 30));
        npmCb.addActionListener(this);
        this.add(Box.createRigidArea(new Dimension(0,10)));
        this.add(npmCb, this);

        // List of all buttons
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(lihat = new JButton("Lihat"));
        lihat.setBackground(Color.decode("#abc32f")); // set color for lihat button
        buttons.add(kembali = new JButton("Kembali"));
        kembali.setBackground(Color.decode("#4890a8")); // set color for kembali button

        this.add(Box.createRigidArea(new Dimension(0,20))); // add gap between text field and button
        // Iterate each button, add spesifications
        for (int i=0; i<buttons.size(); i++) {
            (buttons.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (buttons.get(i)).setFocusable(false);
            (buttons.get(i)).setForeground(Color.WHITE);
            (buttons.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            (buttons.get(i)).addActionListener(this);
            this.add(buttons.get(i), this);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
        }
    }
    
    private void insertionSort(Long[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int j = i;
            while(j > 0 && arr[j] < arr[j-1]) {
                Long temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
                j--;
            }
        }
    }

    private Mahasiswa getMahasiswa(long npm) {
        ArrayList<Mahasiswa> daftarMahasiswa = SistemAkademikGUI.getDaftarMahasiswa();
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource()==kembali) {
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "home");
        } else if (npmCb.getItemCount()==0) {
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
        } else {
            if (e.getSource()==lihat) {
                long npm = (long) npmCb.getSelectedItem();
                Mahasiswa mahasiswa = getMahasiswa(npm);
                (SistemAkademikGUI.panel).add(new DetailRingkasanMahasiswaGUI(SistemAkademikGUI.frame, mahasiswa, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "detail mhs");
                (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
                (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "detail mhs");
            }
        }
    }
}
