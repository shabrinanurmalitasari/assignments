package assignments.assignment4.frontend;

import javax.swing.border.EmptyBorder;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI extends JPanel implements ActionListener {
    private JButton tambahMahasiswa;
    private JButton tambahMataKuliah;
    private JButton tambahIRS;
    private JButton hapusIRS;
    private JButton ringkasanMahasiswa;
    private JButton ringkasanMataKuliah;
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        // TODO: Implementasikan Halaman Home
        this.setBorder(new EmptyBorder(100, 100, 100, 100));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(titleLabel);

        // List of all the buttons
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(tambahMahasiswa = new JButton("Tambah Mahasiswa"));
        buttons.add(tambahMataKuliah = new JButton("Tambah Mata Kuliah"));
        buttons.add(tambahIRS = new JButton("Tambah IRS"));
        buttons.add(hapusIRS = new JButton("Hapus IRS"));
        buttons.add(ringkasanMahasiswa = new JButton("Lihat Ringkasan Mahasiswa"));
        buttons.add(ringkasanMataKuliah = new JButton("Lihat Ringkasan Mata Kuliah"));

        // Iterate each button, add spesifications
        for (int i=0; i<buttons.size(); i++) {
            (buttons.get(i)).setAlignmentX(Component.CENTER_ALIGNMENT);
            (buttons.get(i)).setFocusable(false);
            (buttons.get(i)).setBackground(Color.decode("#abc32f"));
            (buttons.get(i)).setForeground(Color.WHITE);
            (buttons.get(i)).setFont(SistemAkademikGUI.fontGeneral);
            (buttons.get(i)).addActionListener(this);
            this.add(Box.createRigidArea(new Dimension(0,10))); // add gap between buttons
            this.add(buttons.get(i), this);
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==tambahMahasiswa) {
            (SistemAkademikGUI.panel).add(new TambahMahasiswaGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "tambah mahasiswa");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "tambah mahasiswa");

        } else if (e.getSource()==tambahMataKuliah) {
            (SistemAkademikGUI.panel).add(new TambahMataKuliahGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "tambah matakuliah");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "tambah matakuliah");

        } else if (e.getSource()==tambahIRS) {
            (SistemAkademikGUI.panel).add(new TambahIRSGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "tambah IRS");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "tambah IRS");

        } else if (e.getSource()==hapusIRS) {
            (SistemAkademikGUI.panel).add(new HapusIRSGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "hapus IRS");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "hapus IRS");

        } else if (e.getSource()==ringkasanMahasiswa) {
            (SistemAkademikGUI.panel).add(new RingkasanMahasiswaGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "ringkasan mhs");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "ringkasan mhs");

        } else if (e.getSource()==ringkasanMataKuliah) {
            (SistemAkademikGUI.panel).add(new RingkasanMataKuliahGUI(SistemAkademikGUI.frame, SistemAkademikGUI.getDaftarMahasiswa(), SistemAkademikGUI.getDaftarMatakuliah()), "ringkasan matkul");
            (SistemAkademikGUI.frame).add(SistemAkademikGUI.panel);
            (SistemAkademikGUI.cl).show(SistemAkademikGUI.panel, "ringkasan matkul");
        }
    }
}
