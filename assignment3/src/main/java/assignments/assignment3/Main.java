package assignments.assignment3;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

    static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    static int totalMataKuliah = 0;
    static int totalElemenFasilkom = 0;

    // return objek ElemenFasilkom dari nama
    static ElemenFasilkom getObject(String nama) {
        for (ElemenFasilkom ef : daftarElemenFasilkom) {
            if ((ef.nama).equals(nama)) {
                return ef;
            }
        }
        return null;
    }

    // return objek MataKuliah dari nama
    static MataKuliah getMatkul(String nama) {
        for (MataKuliah matkul : daftarMataKuliah) {
            if ((matkul.getNama()).equals(nama)) {
                return matkul;
            }
        }
        return null;
    }

    // return objek MataKuliah dari dosen
    static MataKuliah getMatkul(Dosen dosen) {
        for (MataKuliah matkul : daftarMataKuliah) {
            if ((matkul.getDosen()) == (dosen)) {
                return matkul;
            }
        }
        return null;
    }

    // menambah objek Mahasiswa ke daftarElemenFasilkom
    static void addMahasiswa(String nama, long npm) {
        Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = mahasiswa;
                totalElemenFasilkom += 1;
                break;
            }
        }
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    // menambah objek Dosen ke daftarElemenFasilkom
    static void addDosen(String nama) {
        Dosen dosen = new Dosen(nama);
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = dosen;
                totalElemenFasilkom += 1;
                break;
            }
        }
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    // menambah objek ElemenKantin ke daftarElemenFasilkom
    static void addElemenKantin(String nama) {
        ElemenKantin elemenKantin = new ElemenKantin(nama);
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = elemenKantin;
                totalElemenFasilkom += 1;
                break;
            }
        }
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    // objek1 menyapa objek2 dan sebaliknya
    static void menyapa(String objek1, String objek2) {
        ElemenFasilkom ob1 = getObject(objek1);
        ElemenFasilkom ob2 = getObject(objek2);

        // handle jika ketentuan tidak dipenuhi
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else if (ob1.sudahMenyapa(ob2)) {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", ob1.nama, ob2.nama);
        } else if (ob2.sudahMenyapa(ob1)) {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", ob2.nama, ob1.nama);
        } else {
            ob1.menyapa(ob2);
            ob2.menyapa(ob1);
            System.out.printf("%s menyapa dengan %s\n", objek1, objek2);
        }
    }

    // menambahkan objek Makanan kedalam objek ElemenKantin
    static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob.tipe).equals("ElemenKantin"))) {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
        } else {
            ((ElemenKantin) ob).setMakanan(namaMakanan, harga);
        }
    }

    // objek1 membeli makanan yang dijual objek2
    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom ob1 = getObject(objek1);
        ElemenFasilkom ob2 = getObject(objek2);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob2.tipe).equals("ElemenKantin"))) {
            System.out.printf("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan\n");
        } else if (objek1.equals(objek2)) {
            System.out.printf("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri\n");
        } else if (!((ElemenKantin) ob2).isContainsFood(namaMakanan)) {
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", objek2, namaMakanan);
        } else {
            ob1.membeliMakanan(ob1, ob2, namaMakanan);
        }
    }

    // membuat suatu objek MataKuliah dan memasukkannya ke daftarMataKuliah
    static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {
                daftarMataKuliah[i] = matkul;
                totalMataKuliah += 1;
                System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
                break;
            }
        }
    }

    // menambahkan objek MataKuliah kedalam objek Mahasiswa; sebaliknya
    static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob.tipe).equals("Mahasiswa"))) {
            System.out.printf("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul\n");
        } else {
            Mahasiswa mahasiswa = (Mahasiswa) ob;
            MataKuliah mataKuliah = getMatkul(namaMataKuliah);

            if (mahasiswa.isContainsMatkul(namaMataKuliah)) {
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", namaMataKuliah);
            } else if (mataKuliah.isFull()) {
                System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", namaMataKuliah);
            } else {
                mahasiswa.addMatkul(mataKuliah);
                mataKuliah.addMahasiswa(mahasiswa);
            }
        }
    }

    // drop objek MataKuliah dari objek Mahasiswa; sebaliknya
    static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob.tipe).equals("Mahasiswa"))) {
            System.out.printf("[DITOLAK] Hanya mahasiswa yang dapat drop matkul\n");
        } else {
            Mahasiswa mahasiswa = (Mahasiswa) ob;
            if (!(mahasiswa.isContainsMatkul(namaMataKuliah))) {
                System.out.printf("[DITOLAK] %s belum pernah diambil\n", namaMataKuliah);
            } else {
                MataKuliah mataKuliah = getMatkul(namaMataKuliah);

                mahasiswa.dropMatkul(mataKuliah);
                mataKuliah.dropMahasiswa(mahasiswa);
            }
        }
    }

    // menambah objek MataKuliah ke dalam objek Dosen; sebaliknya
    static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob.tipe).equals("Dosen"))) {
            System.out.printf("[DITOLAK] Hanya dosen yang dapat mengajar matkul\n");
        } else {
            Dosen dosen = (Dosen) ob;
            MataKuliah mataKuliah = getMatkul(namaMataKuliah);

            if (dosen.getMataKuliah() != null) {
                System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", objek, dosen.getMataKuliah());
            } else if (mataKuliah.getDosen() != null) {
                System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", namaMataKuliah);
            } else {
                dosen.mengajarMataKuliah(mataKuliah);
                mataKuliah.addDosen(dosen);
            }
        }
    }

    // menghapus objek MataKuliah ke dalam objek Dosen; sebaliknya
    static void berhentiMengajar(String objek) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika ketentuan tidak dipenuhi
        if (!((ob.tipe).equals("Dosen"))) {
            System.out.printf("[DITOLAK] Hanya dosen yang dapat berhenti mengajar\n");
        } else {
            Dosen dosen = (Dosen) ob;
            
            if (dosen.getMataKuliah() == null) {
                System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", objek);
            } else {
                MataKuliah mataKuliah = getMatkul(dosen);
                
                dosen.dropMataKuliah();
                mataKuliah.dropDosen();
            }
        }
    }

    // mencetak ringkasan objek Mahasiswa
    static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom ob = getObject(objek);

        // handle jika objek bukan Mahasiswa
        if (!((ob.tipe).equals("Mahasiswa"))) {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", objek);
        } else {
            Mahasiswa mahasiswa = (Mahasiswa) ob;

            System.out.println("Nama: " + mahasiswa);
            System.out.println("Tanggal lahir: " + mahasiswa.getTanggalLahir());
            System.out.println("Jurusan: " + mahasiswa.getJurusan());
            System.out.println("Daftar Mata Kuliah:");

            // Cetak daftar kuliah dan handle apabila belum ada mata kuliah yang diambil
            if (mahasiswa.getJumlahMatkul() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                MataKuliah[] daftarMatkul = mahasiswa.getDaftarMatkul();
                int j = 0;
                for (int i = 0; i < mahasiswa.getJumlahMatkul(); i++) {
                    System.out.printf("%d. %s\n", j+=1, daftarMatkul[i]);
                }
            }   
        }
    }

    // mencetak ringkasan objek MataKuliah
    static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah mataKuliah = getMatkul(namaMataKuliah);

        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Dosen pengajar: " + ((mataKuliah.getDosen() == null) ? "Belum ada" : mataKuliah.getDosen())) ;
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");

        // Cetak daftar kuliah dan handle apabila belum ada mata kuliah yang diambil
        if (mataKuliah.getJumlahMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();
            int j = 0;
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                System.out.printf("%d. %s\n", j+=1, daftarMahasiswa[i]);
            }
        }
    }

    // reset array telahMenyapa dan kalkulasi friendship milik objek ElemenFasilkom
    static void nextDay() {
        int total = totalElemenFasilkom - 1;
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                break;
            } else {
                if (daftarElemenFasilkom[i].sapa >= (total/2)) {
                    daftarElemenFasilkom[i].friendship += 10;
                    // validasi agar friendship tidak >100
                    if (daftarElemenFasilkom[i].friendship > 100) {
                        daftarElemenFasilkom[i].friendship = 100; 
                    } 
                } else {
                    daftarElemenFasilkom[i].friendship -= 5;
                    // validasi agar friendship tidak <0
                    if (daftarElemenFasilkom[i].friendship < 0) {
                        daftarElemenFasilkom[i].friendship = 0;
                    }
                }
                daftarElemenFasilkom[i].resetMenyapa();
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        // Cetak ranking sesuai urutan nilai friendship dan nama
        friendshipRanking();
    }

    // sortir objek-objek ElemenFasilkom agar terurut sesuai nilai dan nama
    static void friendshipRanking() {
        ElemenFasilkom[] ef = new ElemenFasilkom[totalElemenFasilkom];
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i] == null) break;
            ef[i] = daftarElemenFasilkom[i];
        }

        Arrays.sort(ef);
        int j = 0;
        for (int i = 0; i < totalElemenFasilkom; i++) {
            System.out.printf("%d. %s(%d)\n", j+=1, ef[i], ef[i].friendship);
        }
    }

    // memberhentikan program dan cetak status peringkat terakhir
    static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada fasilkom.");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}