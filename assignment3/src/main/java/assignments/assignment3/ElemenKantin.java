package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10];

    ElemenKantin(String nama) {
        this.nama = nama;
        this.tipe = "ElemenKantin";
    }

    // check apakah memiliki suatu objek Makanan
    boolean isContainsFood(String namaMakanan) {
        boolean contains = false;
        for (Makanan makanan: daftarMakanan) {
            if (makanan == null) break;
            if ((makanan.getNama()).equals(namaMakanan)) {
                contains = true;
            }
        }
        return contains;
    }

    // return objek Makanan yang dimiliki
    Makanan getMakanan(String namaMakanan) {
        Makanan food = null;
        for (Makanan makanan: daftarMakanan) {
            if (makanan == null) break;
            if ((makanan.getNama()).equals(namaMakanan)) {
                food = makanan;
            }
        }
        return food;
    }

    // menambah objek Makanan
    void setMakanan(String nama, long harga) {
        if (isContainsFood(nama)) {
            System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
        } else {
            for (int i = 0; i < daftarMakanan.length; i++) {
                if (daftarMakanan[i] == null) {
                    daftarMakanan[i] = new Makanan(nama, harga);
                    System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this.nama, daftarMakanan[i].getNama(), daftarMakanan[i].getHarga());
                    break;
                }
            }
        }

    }

    // implementasi abstract method menyapa
    @Override
    void menyapa(ElemenFasilkom elemenFasilkom) {
        for (int i = 0; i < this.telahMenyapa.length; i++) {
            if (this.telahMenyapa[i] == null) {
                this.telahMenyapa[i] = elemenFasilkom;
                break;
            }
        }
        this.sapa += 1;
    }
}