package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    Dosen(String nama) {
        this.nama = nama;
        this.tipe = "Dosen";
    }

    // getter MataKuliah
    MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }

    // setter MataKuliah
    void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    // menambahkan objek MataKuliah
    void mengajarMataKuliah(MataKuliah mataKuliah) {
        setMataKuliah(mataKuliah);
        System.out.printf("%s mengajar mata kuliah %s\n", this.nama, mataKuliah);
    }

    // menghapus objek MataKuliah yang dimiliki
    void dropMataKuliah() {
        System.out.printf("%s berhenti mengajar %s\n", this.nama, getMataKuliah());
        setMataKuliah(null);
    }

    // implementasi abstract method menyapa
    @Override
    void menyapa(ElemenFasilkom elemenFasilkom) {
        for (int i = 0; i < this.telahMenyapa.length; i++) {
            if (this.telahMenyapa[i] == null) {
                this.telahMenyapa[i] = elemenFasilkom;
                break;
            }
        }
        this.sapa += 1;

        // apabila menyapa objek Mahasiswa yang terhubung dalam suatu MataKuliah, friendship bertambah 2
        if (((elemenFasilkom).tipe).equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;

            if (getMataKuliah() != null) {
                if (mahasiswa.isContainsMatkul(getMataKuliah().getNama())) {
                    this.friendship += 2;
                }
            }
        }
    }
}