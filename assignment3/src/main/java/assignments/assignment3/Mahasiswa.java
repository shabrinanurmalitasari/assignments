package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private long npm;
    private String tanggalLahir;
    private String jurusan;
    private int jumlahMatkul; // untuk menyimpan banyak matkul yang dimiliki

    Mahasiswa(String nama, long npm) {
        this.nama = nama;
        this.npm = npm;
        this.tipe = "Mahasiswa";
    }

    // check apakah suatu MataKuliah dimiliki Mahasiswa
    boolean isContainsMatkul(String namaMatkul) {
        boolean contains = false;
        for (MataKuliah matkul: daftarMataKuliah) {
            if (matkul == null) break;
            if ((matkul.getNama()).equals(namaMatkul)) {
                contains = true;
            }
        }
        return contains;
    }

    // menambahkan objek MataKuliah ke dalam daftarMataKuliah
    void addMatkul(MataKuliah mataKuliah) {
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {
                daftarMataKuliah[i] = mataKuliah;
                this.jumlahMatkul += 1;
                System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.nama, mataKuliah.getNama());
                break;
            }
        }
    }

    // menghapus objek MataKuliah ke dalam daftarMataKuliah
    void dropMatkul(MataKuliah mataKuliah) {
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) break;
            if (daftarMataKuliah[i].equals(mataKuliah)){
                for(int j = i; j < daftarMataKuliah.length - 1; j++) {
                    daftarMataKuliah[j] = daftarMataKuliah[j+1];
                }
                // mengisi elemen terakhir setelah shift dengan null
                daftarMataKuliah[daftarMataKuliah.length - 1] = null;
                this.jumlahMatkul -= 1;
                System.out.printf("%s berhasil drop mata kuliah %s\n", this.nama, mataKuliah.getNama());
                break;
            }
        }
    }

    // return string tanggal lahir yang sudah terformat
    String extractTanggalLahir(long npm) {
        String npmString = String.valueOf(npm);
        String birthString = "";
        for (int i = 4; i < 12; i++) {
            char c = npmString.charAt(i);
            birthString += c;
        }

        String d = birthString.substring(0, 2);
        int i = Integer.parseInt(d);
        if (i < 10) {
            d = birthString.substring(1, 2);
        }

        String m = birthString.substring(2, 4);
        int j = Integer.parseInt(m);
        if (j < 10) {
            m = birthString.substring(3, 4);
        }

        String y = birthString.substring(4);
        String dmy = d + "-" + m + "-" + y;

        return dmy;
    }

    // return jurusan Mahasiswa
    String extractJurusan(long npm) {
        String npmString = String.valueOf(npm);
        String major = "";
        for (int i = 2; i < 4; i++) {
            char c = npmString.charAt(i);
            major += c;
        }

        if (major.equals("01")) {
            major = "Ilmu Komputer";
        } else if (major.equals("02")) {
            major = "Sistem Informasi";
        }
        return major;
    }

    // getter tanggalLahir
    String getTanggalLahir() {
        this.tanggalLahir = extractTanggalLahir(this.npm);
        return this.tanggalLahir;
    }

    // getter jurusan
    String getJurusan() {
        this.jurusan = extractJurusan(this.npm);
        return this.jurusan;
    }

    // getter daftarMataKuliah
    MataKuliah[] getDaftarMatkul() {
        return this.daftarMataKuliah;
    }

    // return jumlah MataKuliah yang dimiliki Mahasiswa
    int getJumlahMatkul() {
        return this.jumlahMatkul;
    }

    // implementasi abstract method menyapa
    @Override
    void menyapa(ElemenFasilkom elemenFasilkom) {
        for (int i = 0; i < this.telahMenyapa.length; i++) {
            if (this.telahMenyapa[i] == null) {
                this.telahMenyapa[i] = elemenFasilkom;
                break;
            }
        }
        this.sapa += 1;

        // apabila menyapa objek Dosen yang terhubung dalam suatu MataKuliah, friendship bertambah 2
        if ((elemenFasilkom.tipe).equals("Dosen")) {
            Dosen dosen = (Dosen) elemenFasilkom;
            MataKuliah matkul = (MataKuliah) dosen.getMataKuliah();

            if (matkul != null) {
                if (isContainsMatkul(matkul.getNama())) {
                    this.friendship += 2;
                }
            }
        }
    }
}