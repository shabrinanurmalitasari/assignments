package assignments.assignment3;

class MataKuliah {

    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa; // untuk simpan jumlah Mahasiswa yang dimiliki

    MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // getter nama
    String getNama() {
        return this.nama;
    }

    // getter kapasitas
    int getKapasitas() {
        return this.kapasitas;
    }

    // getter dosen
    Dosen getDosen() {
        return this.dosen;
    }

    // getter array daftarMahasiswa
    Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    // getter jumlah mahasiswa yang dimiliki
    int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    // check apakah kapasitas matkul sudah penuh
    boolean isFull() {
		if (this.jumlahMahasiswa == this.kapasitas) {
            return true;
        } else {
            return false;
        }
	}

    // menambah objek Mahasiswa ke dalam daftarMahasiswa
    void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) {
                daftarMahasiswa[i] = mahasiswa;
                jumlahMahasiswa += 1;
                break;
            }
        }
    }

    // menghapus objek Mahasiswa dari daftarMahasiswa
    void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) break;
            if (daftarMahasiswa[i].equals(mahasiswa)) {
                for(int j = i; j < daftarMahasiswa.length - 1; j++) {
                    daftarMahasiswa[j] = daftarMahasiswa[j+1];
                }
                // mengisi elemen terakhir setelah shift dengan null
                daftarMahasiswa[daftarMahasiswa.length - 1] = null;
                jumlahMahasiswa -= 1;
                break;
            }
        }
    }

    // set dosen
    void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    // menghapus dosen
    void dropDosen() {
        this.dosen = null;
    }

    // return nama
    public String toString() {
        return getNama();
    }
}