package assignments.assignment3;

abstract class ElemenFasilkom implements Comparable<ElemenFasilkom> {
    
    String tipe;
    String nama;
    int friendship;
    ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    int sapa; // menghitung berapa banyak ElemenFasilkom yang disapa

    // check apakah sudah menyapa suatu ElemenFasilkom
    boolean sudahMenyapa(ElemenFasilkom elemenFasilkom) {
        boolean contains = false;
        for (ElemenFasilkom ef: telahMenyapa) {
            if (ef == null) break;
            if (ef == elemenFasilkom) {
                contains = true;
            }
        }
        return contains;
    }

    // abstract method untuk menyapa; impelementasi berbeda di setiap child class
    abstract void menyapa(ElemenFasilkom elemenFasilkom);

    // reset daftar objek yang telah disapa
    void resetMenyapa() {
        this.telahMenyapa = new ElemenFasilkom[100];
        this.sapa = 0;
    }

    // membeli makanan dari objek lain
    void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        Makanan makanan = ((ElemenKantin) penjual).getMakanan(namaMakanan);
        System.out.printf("%s berhasil membeli %s seharga %d\n", this.nama, namaMakanan, makanan.getHarga());
        // menambah friendship pembeli maupun penjual sebanyak 1
        this.friendship += 1;
        penjual.friendship += 1;
    }

    // return nama objek
    public String toString() {
        return this.nama;
    }

    // override method compareTo dari Comparable
    // agar objek dapat disortir dari nilai friendship dari tinggi ke rendah serta alphabetical
    @Override
    public int compareTo(ElemenFasilkom other) {
        int friendshipResult = -1 * Integer.compare(friendship, other.friendship);
        return friendshipResult != 0 ? friendshipResult : nama.compareTo(other.nama);
    }
}