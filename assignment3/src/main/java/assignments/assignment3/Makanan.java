package assignments.assignment3;

class Makanan {

    private String nama;
    private long harga;

    Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    // mengembalikan nama
    public String toString() {
        return getNama();
    }

    // getter nama
    String getNama() {
        return this.nama;
    }

    // getter harga
    long getHarga() {
        return this.harga;
    }
}