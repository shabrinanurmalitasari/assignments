package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // return nama MataKuliah
    public String getNama() {
        return this.nama;
    }

    // return kode MataKuliah
    public String getKode() {
        return this.kode;
    }

    // return sks MataKuliah
    public int getSKS() {
        return this.sks;
    }

    // return kapasitas MataKuliah
    public int getKapasitas() {
        return this.kapasitas;
    }

    // return array daftar mahasiswa MataKuliah
    public Mahasiswa[] getDaftarMhs() {
        return daftarMahasiswa;
    }

    // menghitung jumlah mahasiswa yang dimiliki MataKuliah
    public int banyakMahasiswa() {
        int banyakMhs = 0;
        for (Mahasiswa mhs: daftarMahasiswa) {
            if (mhs == null) break;
            banyakMhs += 1;
        }
        return banyakMhs;
    }

    // mengecek apakah kapasitas MataKuliah sudah penuh
    public boolean isFull() {
        boolean full = false;
        if (banyakMahasiswa() == kapasitas) {
            full = true;
        }
        return full;
    }


    // menambah mahasiswa kedalam array daftar mahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) {
                daftarMahasiswa[i] = mahasiswa;
                break;
            }
        }
    }

    // remove mahasiswa dalam array daftar mahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) break;
            if (daftarMahasiswa[i].equals(mahasiswa)) {
                for(int j = i; j < daftarMahasiswa.length - 1; j++) {
                    daftarMahasiswa[j] = daftarMahasiswa[j+1];
                }
                // mengisi elemen terakhir setelah shift dengan null
                daftarMahasiswa[daftarMahasiswa.length - 1] = null;
                break;
            }
        }
    }

    // mengembalikan nama MataKuliah
    public String toString() {
        return getNama();
    }
}
