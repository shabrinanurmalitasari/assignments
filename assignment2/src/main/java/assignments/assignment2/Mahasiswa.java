package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.masalahIRS = new String[20];
    }

    // return nama Mahasiswa
    public String getNama() {
        return this.nama;
    }

    // return npm Mahasiwa
    public long getNPM() {
        return this.npm;
    }

    // return daftarMatkul Mahasiswa
    public MataKuliah[] getDaftarMatkul() {
        return this.mataKuliah;
    }

    // return masalahIRS Mahasiswa
    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }

    // menghitung total masalah IRS Mahasiswa
    public int getTotalIRS() {
        int masalah = 0;
        for (String IRS: masalahIRS) {
            if (IRS == null) break;
            masalah += 1;
        }
        return masalah;
    }

    // return totalSKS Mahasiswa
    public int getTotalSKS() {
        return totalSKS;
    }

    // menghitung banyak mata kuliah yang dimiliki Mahasiswa
    public int getBanyakMatkul() {
        int banyakMatkul = 0;
        for (MataKuliah matkul: mataKuliah) {
            if (matkul == null) break;
            banyakMatkul += 1;
        }
        return banyakMatkul;
    }

    // return string jurusan Mahasiswa
    public String getMajor() {
        String npmString = String.valueOf(npm);
        jurusan = "";
        for (int i = 2; i < 4; i++) {
            char c = npmString.charAt(i);
            jurusan += c;
        }

        if (jurusan.equals("01")) {
            jurusan = "Ilmu Komputer";
        } else if (jurusan.equals("02")) {
            jurusan = "Sistem Informasi";
        }
        return jurusan;
    }

    // return string kode jurusan Mahasiswa (IK/CS)
    public String getMajorCode() {
        String npmString = String.valueOf(npm);
        jurusan = "";
        for (int i = 2; i < 4; i++) {
            char c = npmString.charAt(i);
            jurusan += c;
        }

        if (jurusan.equals("01")) {
            jurusan = "IK";
        } else if (jurusan.equals("02")) {
            jurusan = "SI";
        }
        return jurusan;
    }

    // mengecek apakah suatu matkul dimiliki oleh Mahasiswa
    public boolean isContainsMatkul(String namaMatkul) {
        boolean contains = false;
        for (MataKuliah matkul: mataKuliah) {
            if (matkul == null) break;
            if ((matkul.getNama()).equals(namaMatkul)) {
                contains = true;
            }
        }
        return contains;
    }
    
    // menambah suatu matkul dan sksnya
    public void addMatkul(MataKuliah matkul){
        for (int i = 0; i < mataKuliah.length; i++) {
            if (mataKuliah[i] == null) {
                mataKuliah[i] = matkul;
                totalSKS += matkul.getSKS();
                break;
            }
        }
    }

    // menghapus suatu matkul dan sksnya
    public void dropMatkul(MataKuliah matkul){
        for (int i = 0; i < mataKuliah.length; i++) {
            if (mataKuliah[i] == null) break;
            if (mataKuliah[i].equals(matkul)) {
                totalSKS -= mataKuliah[i].getSKS();
                for(int j = i; j < mataKuliah.length - 1; j++) {
                    mataKuliah[j] = mataKuliah[j+1];
                }
                // mengisi elemen terakhir setelah shift dengan null
                mataKuliah[mataKuliah.length - 1] = null;
                break;
            }
        }
    }

    // validasi IRS, menambah masalah kedalam masalahIRS jika ada
    public void cekIRS(){
        int i = 0;
        if (getTotalSKS() > 24) {
            masalahIRS[i] = "SKS yang Anda ambil lebih dari 24";
            i++;
        }

        for (int j = 0; j < getBanyakMatkul(); j++) {
            if (!mataKuliah[j].getKode().equals("CS")) {
                if (!mataKuliah[j].getKode().equals(getMajorCode())) {
                    masalahIRS[i] = "Mata Kuliah " + mataKuliah[j].getNama() + " tidak dapat diambil jurusan " + getMajorCode();
                    i++;
                    }
            }
        }
    }

    // return nama Mahasiswa
    public String toString() {
        return getNama();
    }

}
