package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    // get year code
    public static int getYear(long npm) {
        String npmString = String.valueOf(npm);
        String yearString = "";
        for (int i = 0; i < 2; i++) {
            char c = npmString.charAt(i);
            yearString += c;
        }
        int year = Integer.parseInt(yearString);
        return year;
    }

    // get major code
    public static int getMajor(long npm) {
        String npmString = String.valueOf(npm);
        String majorString = "";
        for (int i = 2; i < 4; i++) {
            char c = npmString.charAt(i);
            majorString += c;
        }
        int major = Integer.parseInt(majorString);
        return major;
    }

    // check if major exist
    public static boolean isMajorExist(int majorCode) {
        boolean isExist = false;

        switch (majorCode) {
            case 1:
                isExist = true;
            case 2:
                isExist = true;
            case 3:
                isExist = true;
            case 11:
                isExist = true;
            case 12:
                isExist = true;
        }
        return isExist;
    }

    // get ddmmyyyy string for birth
    public static String getBirthString(long npm) {
        String npmString = String.valueOf(npm);
        String birthString = "";
        for (int i = 4; i < 12; i++) {
            char c = npmString.charAt(i);
            birthString += c;
        }
        return birthString;
    }

    // get age derived from subtraction
    public static int getAge(long npm) {
        String birthString = getBirthString(npm);
        String yearString = "";
        int year = 2000 + getYear(npm);
        for (int i = 4; i < birthString.length(); i++) {
            char c = birthString.charAt(i);
            yearString += c;
        }
        int age = year - Integer.parseInt(yearString);
        return age;
    }

    // get code out of the first 13 digit of npm
    public static int getCode(long npm) {
        String npmString = String.valueOf(npm);
        String codeString = "";
        int code = 0;
        for (int i = 0; i < 13; i++) {
            char c = npmString.charAt(i);
            codeString += c;
        }

        String a = "";
        String b = "";

        // first digits
        for (int i = 0; i < 6; i++) {
            a += codeString.charAt(i);
        }

        // last digits
        for (int j = 12; j >= 7; j--) {
            b += codeString.charAt(j);
        }

        // multiply every first-last digits
        for (int i = 0; i < 6; i++) {
            int x = Character.getNumericValue(a.charAt(i));
            int y = Character.getNumericValue(b.charAt(i));
            code += x * y;
        }
        // add with the 7th number (middle number)
        code += Character.getNumericValue(codeString.charAt(6));

        while (code >= 10) {
            code = add(code);
        }
        return code;
    }

    // recursive function to add digits in a number
    public static int add(int n) {
        int sum = n % 10;
        if (n == 0) {
            return 0;
        } else {
            return sum + add(n / 10);
        }
    }

    // validate length, major code, age, npm code
    public static boolean validate(long npm) {
        boolean valid = false;
        int length = String.valueOf(npm).length();

        if (length == 14) {
            long lastDigit = Math.abs(npm % 10);
            int major = getMajor(npm);
            int age = getAge(npm);
            int code = getCode(npm);
            if (isMajorExist(major)) {
                if (age >= 15) {
                    if (code == lastDigit) {
                        valid = true;   
                    }
                }
            }
        } else {
            // do nothing, valid is already setted at false
        }
        return valid;
    }

    // to extract year, major, birth information from npm
    public static String extract(long npm) {
        int year = 2000 + getYear(npm);
        int majorCode = getMajor(npm);
        String birthString = getBirthString(npm);
        String d = birthString.substring(0, 2);
        String m = birthString.substring(2, 4);
        String y = birthString.substring(4);
        String dmy = d + "-" + m + "-" + y;
        String major = "";

        if (majorCode == 1) {
            major = "Ilmu Komputer";
        } else if (majorCode == 2) {
            major = "Sistem Informasi";
        } else if (majorCode == 3) {
            major = "Teknologi Informasi";
        } else if (majorCode == 11) {
            major = "Teknik Telekomunikasi";
        } else if (majorCode == 12) {
            major = "Teknik Elektro";
        }

        return "Tahun masuk: " + year + "\n" + "Jurusan: " + major + "\n" + "Tanggal Lahir: " + dmy;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;

        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            
            if (validate(npm)) {
                System.out.print(extract(npm));
            } else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}